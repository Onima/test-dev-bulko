<?php
function autoload($class) {
    require_once str_replace("\\", "/", $class) . '.class.php';
}
spl_autoload_register('autoload');

ini_set('display_errors', 1);
ini_set('display_startup_errors',1);
error_reporting(-1);

define('MYSQL_HOST', 'localhost');
define('MYSQL_BASE', 'test_bko');
define('MYSQL_USER', 'root');
define('MYSQL_PASS', 'root');
define('BASE_HOST', '/test-dev-bulko/');