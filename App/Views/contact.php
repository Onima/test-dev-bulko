<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<title>TestBulko - 2017</title>
	<meta name="viewport" content="user-scalable=no, initial-scale = 1, minimum-scale = 1, maximum-scale = 1, width=device-width">
	<link rel="icon" type="image/vnd.microsoft.icon" href="http://www.bulko.net/templates/img/favicon.ico" />
	<link rel="shortcut icon" type="image/x-icon" href="http://www.bulko.net/templates/img/favicon.ico" />
	<link rel="stylesheet" href="https://cdn.bootcss.com/meyer-reset/2.0/reset.min.css">
	<link rel="stylesheet" href="<?=BASE_HOST;?>asset/css/styles.css">
</head>
<body>
	<header>
		<div class="wrapper">
			<a class="logo-bulko" href="http://www.bulko.net/" title="Logo Agence Bulko"><img src="<?=BASE_HOST;?>asset/img/logoBulko.png" alt="Logo Agence Bulko" ></a>
			<a class="logo-github" href="https://github.com/Bulko/test-dev-bulko/blob/master/README.md" title="Lire les consignes" target="_blank" rel="noopener">
				<img src="<?=BASE_HOST;?>asset/img/github-icon.png" alt="Logo github">README.md
			</a>
		</div>
	</header>
	<main>
		<div class="form-ok hidden"></div>
		<div class="form-error hidden"></div>
		<form method="post">
			<p>Contactez-nous</p>
			<div class="form-part-1">
				<div class="form-control">
					<input type="text" name="nom" placeholder="Nom"/>
				</div>
				<div class="form-control">
					<input type="email" name="email" placeholder="Email"/ required="required">
				</div>
				<div class="form-control">
					<input type="tel" name="tel" placeholder="Téléphone"/ required="required">
				</div>
			</div>
			<div class="form-part-2">
				<div class="form-control">
					<textarea name="message" placeholder="Message"></textarea>
				</div>
				<input type="submit" value="Envoyer" />
			</div>
		</form>
	</main>
	<footer>
		<p>© Bulko - 2017<br>🦄  GLHF</p>
	</footer>
	<script type="text/javascript">
		function validateEmail(email) {
 			var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    		return re.test(email);
		}

		function validatePhone(tel) {
			tel = tel.replace(/[^0-9]/g, '');
			return tel.length == 10;
		}

		function getXMLHttpRequest() {
	        var xhr = null;
	        if (window.XMLHttpRequest || window.ActiveXObject) {
	            if (window.ActiveXObject)
	                try { xhr = new ActiveXObject("Msxml2.XMLHTTP"); }
	                catch(e) { xhr = new ActiveXObject("Microsoft.XMLHTTP"); }
	            else
	                xhr = new XMLHttpRequest();
	        } else {
	            alert("Votre navigateur ne supporte pas l'objet XMLHTTPRequest...");
	            return null;
	        }
	        return xhr;
	    }


		var json = <?= $json ?>;

		console.log('Status code :', json.status.code, ',', json.status.message);
		if(json.status.code === 200) {
			if(json.message !== undefined) {
				console.log('ID', json.message.id);
				console.log('Nom', json.message.nom);
				console.log('Email', json.message.email);
				console.log('Téléphone', json.message.tel);
				console.log('Message', json.message.message);
			} else if(json.messages !== undefined) {
				json.messages.forEach(function(message){
					console.log('ID', message.id);
					console.log('Nom', message.nom);
					console.log('Email', message.email);
					console.log('Téléphone', message.tel);
					console.log('Message', message.message);
				});
			}
			
		}

		var form = document.querySelector('form');
		var email = document.querySelector('input[name=email]');
		var tel = document.querySelector('input[name=tel]');
		form.addEventListener('submit', function(e) {
			e.preventDefault();
			var error = document.querySelector('.form-error');
			error.innerText = "";
			if(!validateEmail(email.value)) {
				if(error.classList.contains('hidden')) error.classList.remove('hidden');
				error.innerText += 'Votre adresse email n\'est pas valide.';
			}
			if(!validatePhone(tel.value)) {
				if(error.classList.contains('hidden')) error.classList.remove('hidden');
				error.innerText += ' Votre numéro de téléphone n\'est pas valide (10 chiffres sans séparateurs ou ne commençant pas par 0)';
			}

			if(error.innerText == "") {
				if(!error.classList.contains('hidden')) error.classList.add('hidden');
				var formData = new FormData();
				var fields = document.querySelectorAll('form input:not([type=submit]), form textarea');

				for(var i = 0; i < fields.length; i++) {
					formData.append(fields.item(i).name, fields.item(i).value);
				}

				var xhr = getXMLHttpRequest();
				xhr.open('POST', <?=BASE_HOST?>+'/message');
				xhr.onreadystatechange = function() {
			        if (xhr.readyState>3 && xhr.status==200 || xhr.status == 0) { 
			        	var response = JSON.parse(xhr.responseText);
			        	if(response.status.type == 'success') {
			        		var success = document.querySelector('.form-ok');
			        		success.innerText = response.status.message;
			        		if(success.classList.contains('hidden')) success.classList.remove('hidden');
			        		form.reset();
			        	} else if (response.status.type == 'fail') {
			        		if(error.classList.contains('hidden')) error.classList.remove('hidden');
			        		error.innerText = response.status.message;
			        	}
			        }
			    };
			    xhr.send(formData);
			}
		})



	</script>
</body>
</html>