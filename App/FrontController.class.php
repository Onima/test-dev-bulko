<?php 

namespace App;

use App\Core\Route;
use App\Core\Routes;

class FrontController {
	private $routes;

	public function __construct()
	{
		$this->routes = new Routes();
		$this->routes->add(new Route('message', 'Message', 'add', 'POST'));
		$this->routes->add(new Route('message', 'Message', 'showAll', 'GET'));
		$this->routes->add(new Route('message/(\\d+)', 'Message', 'show', 'GET'));
	}

	private function getPath() {
		$path = $_SERVER['REQUEST_URI'];
		$pos = strpos($path, '?');
		if($pos !== false) $path = substr($path, 0, $pos);
		return $path;
	}

	public function run()
	{
		// Analyse de l'URL
		$url = str_replace(BASE_HOST, '', $this->getPath());
		if(empty($url)) $url = 'message';

		// Trouver une route
		$route = $this->routes->find($url);
		if(is_null($route)) $route = $this->routes->get(0);

		// Appeler le controller et l'action correspondant
		$class = 'App\\Controller\\'.$route->getCtrl().'Controller';
		$ctrl = new $class();

		@list($message) = $ctrl->{$route->getAction()}(...$route->getParams());

		$html = ob_get_clean();
		exit($html);
	}
}