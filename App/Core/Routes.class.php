<?php 

namespace App\Core;

class Routes {
	private $routes = [];

	public function add(Route $route)
	{
		$this->routes[] = $route;
	}

	public function count()
	{
		return count($this->routes);
	}

	public function get($index) 
	{
		if($index >= 0 && $index < $this->count()) 
			return $this->routes[$index];
		else 
			return null;
	}

	public function find($url)
	{
		foreach ($this->routes as $route) {
			if(empty($route->getUrl())) continue;
			$reg = '@^'.$route->getUrl().'$@';
			preg_match($reg, $url, $match);
			if(count($match) && $route->isMethod($route->getMethod())) return $route;
		}
		return null;
	}
}

class Route {
	private $url, $ctrl, $action, $method;

	public function __construct($url, $ctrl, $action, $method)
	{
		$this->url = $url;
        $this->ctrl = $ctrl;
        $this->action = $action;
        $this->method = $method;
	}

	public function __toString() {
		return 'Route, url: '.$this->url.' ctrl: '.$this->ctrl.', action: '.$this->action.', method: '.$method;
	}

	public function getUrl()
    {
        return $this->url;
    }

    public function getCtrl()
    {
        return $this->ctrl;
    }

    public function getAction()
    {
        return $this->action;
    }

    public function getMethod()
    {
        return $this->method;
    }

	public function getParams()
    {
        preg_match('@^'.BASE_HOST.$this->url.'$@', $_SERVER['REQUEST_URI'], $matches);
        if(count($matches) > 1) {
            array_shift($matches);
            return $matches;
        }
        return null;
    }

    public static function isMethod($method = 'GET') {
		return $_SERVER['REQUEST_METHOD'] == strtoupper($method);
	}
}