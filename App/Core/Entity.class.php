<?php 

namespace App\Core;

use PDO;
use App\Utils\DB;

abstract class Entity
{
	public static function findAll($params = null)
	{
		$class = get_called_class();
		$qry = "SELECT * FROM ".$class::$TABLE;
		$stt = DB::prepare($qry);
        $stt->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $class);
        $stt->execute();
        return $stt->fetchAll();
	}

	public static function find($id)
	{
		$class = get_called_class();
		$qry = "SELECT * FROM ".$class::$TABLE." WHERE id = :id";

		$stt = DB::prepare($qry);
		$stt->bindParam(':id', $id);
        $stt->setFetchMode(PDO::FETCH_CLASS | PDO::FETCH_PROPS_LATE, $class);
        $stt->execute();
        return $stt->fetch();
	}

	protected $id;

	public function getId() { return $this->id; }
    public function setId($id) { $this->id = $id; }

    public function __construct($data = null) {
        if($data !== null) $this->hydrate($data);
    }

    public function hydrate($data)
    {
        foreach ($data as $key => $value) {
            $m = 'set'.ucfirst($key);
            if(method_exists($this, $m)) $this->$m($value);
        }
    }
}