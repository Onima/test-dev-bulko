<?php 

namespace App\Model;

use App\Core\Entity;
use App\Utils\DB;

class Message extends Entity {
	public static $TABLE = 'message';

	private $nom;
    private $email;
    private $tel;
    private $message;

	public function getNom() { return $this->nom; }
    public function setNom($nom) { $this->nom = $nom; }
    
    public function getEmail() { return $this->email; }
    public function setEmail($email) { $this->email = $email; }
    
    public function getTel() { return $this->tel; }
    public function setTel($tel) { $this->tel = $tel; }
    
    public function getMessage() { return $this->message; }
    public function setMessage($message) { $this->message = $message; }

    public function add()
	{
		$class = get_called_class();
		$qry = "INSERT INTO ".$class::$TABLE. " SET ";
		foreach(get_class_vars($class) as $key => $value) {
			if($key == 'id' || $key == 'TABLE') continue;
			$fields[] = $key . ' = :' .$key;
			$m = 'get'.ucfirst($key);
            if(method_exists($this, $m)) $values[$key] = $this->$m();
		}
		$qry .= implode(', ', $fields);
		$stt = DB::prepare($qry);
		foreach ($values as $key => $value) {
			$type = is_numeric($value) ? \PDO::PARAM_INT : \PDO::PARAM_STR;
			$stt->bindValue($key, $value, $type);
		}
		if($stt->execute()) $this->setId(DB::lastInsertId());
	}

	public function sendMail()
	{
		$line = "\r\n";

        $to = "info@bulko.net";
        $from = $this->email;
        $message = strlen(trim($this->message)) > 0 ? $this->message : "L'utilisateur n'a pas renseigné de message." ;
        $contact = strlen(trim($this->nom)) > 0 ? $this->nom : "Anonyme";
        $tel = $this->tel;


		// Quelques remplacements pour les specialchars  
        $message=preg_replace('#(<|>)#', '-', $message);
        $message=str_replace('"', "'",$message);
        $message=str_replace('&', 'et',$message);

        // On assigne et/ou protege nos variables
        $message=stripslashes(htmlspecialchars($message));

        // On enleve les espaces
        $message=trim($message);

        // Construction de l'email
        $boundary = "-----=".md5(rand());

        $header = "From: \"$contact\" <$from>".$line;
        $header.= "MIME-Version: 1.0".$line;
        $header.= "Content-Type: multipart/alternative;".$line." boundary=\"$boundary\"".$line;

        $corps = $line."--".$boundary.$line;
		$corps.= "Content-Type: text/plain; charset=\"UTF-8\"".$line;
		$corps.= "Content-Transfert-Encoding: 8bit".$line;
        $corps.= $line.$message.$line;
        $corps.= $line."--".$boundary."--".$line;

		return mail($to, 'Test dev Bulko', $corps, $header);
	}

	function validEmail($email) {
		return trim($email) === filter_var($email, FILTER_VALIDATE_EMAIL);
	}

	function validPhone($num) {
		$num = str_replace(['.', '-', ' '], '', $num);
		$prefix = ['06', '07', '01', '02', '03', '04', '05', '09'];
		return strlen($num) == 10 && is_numeric($num) && in_array(substr($num, 0, 2), $prefix);
	}
}