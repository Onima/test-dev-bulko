<?php
namespace App\Utils;

use PDO;
use PDOException;

class DB {
	private static $instance = null;
	public static function getInstance() {
		if (self::$instance == null) {
			try {
				self::$instance = new PDO('mysql:host='.MYSQL_HOST.';dbname='.MYSQL_BASE, MYSQL_USER, MYSQL_PASS);
				self::$instance->setAttribute(PDO::ATTR_CASE, PDO::CASE_LOWER);
			    self::$instance->setAttribute(PDO::ATTR_ERRMODE , PDO::ERRMODE_EXCEPTION);
			    self::$instance->exec("SET NAMES 'utf8'");
			} catch (PDOException $e) {
				die($e->getMessage());
			}
		}
		return self::$instance;
	}

	private function __construct() {}
    private function __clone() {}

	public static function __callStatic($name, $args) {
		$con = self::getInstance();
		try {
			return call_user_func_array(array($con, $name), $args);
		} catch (PDOException $e) {
			echo $e->getMessage();
		}
	}
}