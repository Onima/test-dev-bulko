<?php 

namespace App\Controller;

use App\Model\Message;
use App\Utils\DB;

class MessageController
{
	private $view = './App/Views/contact.php';

	public function showAll()
	{		
		$messages = Message::findAll();
		$array = [];
		$methods = [];

		if(count($messages)) {
			foreach(get_class_methods(get_class($messages[0])) as $method) {
				if(preg_match('/^get/', $method)) $methods[] = $method;
			}
			
			foreach ($messages as $message) {
				foreach ($methods as $m) {
					$key = strtolower(str_replace('get', '', $m));
					$arr[$key] = $message->$m();
				}
				array_push($array, $arr);
			}
		}


		if(count($array) == 0) $json = ['status' => ['code' => 404, 'message' => 'Aucun message trouvé']];
		else $json = array_merge(['status' => ['code' => 200, 'message' => 'Les messages ont été récupérés']], ['messages' => $array]);

		$json = json_encode($json, JSON_UNESCAPED_UNICODE);

		ob_start();
		require $this->view;
	}

	public function show($id)
	{
		$message = Message::find($id);
		if(!$message) $json = ['status' => ['code' => 404, 'message' => 'Aucun message trouvé pour l\'identifiant '.$id]];
		else {
			foreach(get_class_methods(get_class($message)) as $method) {
				if(preg_match('/^get/', $method)) $methods[] = $method;
			}
			
			foreach ($methods as $m) {
				$key = strtolower(str_replace('get', '', $m));
				$arr[$key] = $message->$m();
			}

			$json = array_merge(['status' => ['code' => 200, 'message' => 'Le message a été récupéré']], ['message' => $arr]);
		}

		$json = json_encode($json, JSON_UNESCAPED_UNICODE);

		ob_start();
		require $this->view;
	}

	public function add()
	{
		$message = new Message($_POST);
		
		$message->add();
		$mail = $message->sendMail();
		
		if($mail) {
			$json = ['status' => ['type' => 'success', 'message' => 'Votre email a bien été envoyé ! Nous vous répondrons dans les plus brefs délais.']];
		} else {
			$json = ['status' => ['type' => 'fail', 'message' => 'Il semblerait qu\'il y ait eu une erreur lors de l\'envoi.']];
		}

		echo $json = json_encode($json, JSON_UNESCAPED_UNICODE);

	}

	
}